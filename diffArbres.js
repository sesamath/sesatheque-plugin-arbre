import { toAscii } from 'sesajstools'
import { empty, addElement } from 'sesajstools/dom'
import getUrls from 'sesatheque-client/src/getUrls'
import { fetchPublicItem, fetchEnfants } from 'sesatheque-client/src/simpleClient'
import { addSesatheques } from 'sesatheque-client/src/fetch'
// ça vient de sesatheque
import { sesatheques } from 'server/config'

import loadingImg from './public/images/loading.gif'

import './public/diffArbres.scss'

/*
Algo de comparaison d'arbres.
Pas facile de trouver de la littérature, 'tree diff algorithm' remonte surtout des trucs traitant de git-diff, diff de dossiers de fichiers ou virtual dom.
Y'a quand même
https://medium.com/f-ing-about/f-tree-diff-algorithm-5b7f9c85beac
https://stackoverflow.com/questions/7806176/tree-diff-algorithm

Ça reste quand même vraiment compliqué…

On veut distinguer
- node qui n'existe pas chez l'autre (.orphan => rouge)
- node qui a les mêmes parents et les mêmes voisins (.idem => vert)
- node qui a le même 1er parent et les mêmes voisins (.sameParentAndSiblings => jaune-vert), son parent peut avoir été déplacé, c'est lui qui sera marqué jaune.
- node qui a le même 1er parent mais pas les mêmes voisins (.sameParent => jaune)
- sinon (pas le même 1er parent) (.adopted => orange)
=> cf le code de flagItems qui gère ça
 */

// code exécuté dès son chargement
addSesatheques(sesatheques)

const pictoStr = '🙌' // on avait aussi 👥 👣 ✨

const g = (id) => document.getElementById(id)
const q = (selector) => document.querySelectorAll(selector)
const q1 = (selector) => document.querySelector(selector)

// listes d'items par rid
/** @type {Object<string, ClientItemExtended[]>} */
const items1 = {}
/** @type {Object<string, ClientItemExtended[]>} */
const items2 = {}

/**
 * Affiche une erreur (à l'écran et en console)
 * @param {Error|string} error
 * @param purge
 */
function addError (error, purge = false) {
  if (purge) empty(ctErrors)
  if (!error) return
  if (typeof error === 'string') error = Error(error)
  console.error(error)
  addElement(ctErrors, 'p', {}, error.message)
  ctErrors.scrollIntoView()
}

/**
 * Vide un objet
 * @param {Object} obj
 */
function emptyObj (obj) {
  for (const p of Object.keys(obj)) delete obj[p]
}

async function load (tree1, tree2) {
  try {
    loading.classList.remove('hidden')
    toggleButton.classList.add('hidden')
    // on vide les erreurs
    empty(ctErrors)
    empty(ct1)
    empty(ct2)
    if (tree1.type !== 'arbre') throw Error('L’arbre 1 n’est pas un arbre')
    if (tree2.type !== 'arbre') throw Error('L’arbre2 n’est pas un arbre')
    emptyObj(items1)
    await showArbre(tree1, ct1, items1)
    emptyObj(items2)
    await showArbre(tree2, ct2, items2)
    compare()
    // virer idem sur la racine
    for (const ul of q('#trees td > ul')) {
      ul.classList.remove('idem')
    }
    // reset toggleButton
    if (!isIdemDisplayed) toggleButton.click()
  } catch (error) {
    addError(error)
  }
  loading?.classList.add('hidden')
  toggleButton.classList.remove('hidden')
}

/**
 * Affiche un arbre (et parse tous ses enfants)
 * @param {ClientItem} arbre
 * @param {HTMLElement} ul
 * @param {Object<string, ClientItemExtended[]>} items
 * @return {Promise<void>}
 */
async function showArbre (arbre, ul, items) {
  // on ajoute du li sauf pour la racine
  const ct = ([ct1, ct2].includes(ul))
    ? ul
    : addElement(addElement(ul, 'li'), 'ul')
  const props = {}
  if (arbre.rid) {
    const { displayUrl } = getUrls(arbre)
    props.href = displayUrl
    props.target = '_blank'
  }
  addElement(ct, 'a', props, arbre.titre)
  addItem(ct, arbre, items)
  const enfants = await fetchEnfants(arbre)
  arbre.enfants = [] // reset, on va le remplacer par une liste de ClientItemsExtended
  let previous = null
  for (const enfant of enfants) {
    arbre.enfants.push(enfant)
    enfant.parent = arbre
    enfant.previous = previous
    if (previous) previous.next = enfant
    await addChild(enfant, ct, items)
    previous = enfant
  }
  // le dernier n'a pas de suivant
  previous.next = null
}

/**
 * @typedef ClientItemExtended
 * @extends ClientItem
 * @property {HTMLElement} domElt
 * @property {ClientItemExtended|null} parent
 * @property {ClientItemExtended|null} previous
 * @property {ClientItemExtended|null} next
 * @property {ClientItemExtended|null} twin
 * @property {ClientItemExtended[]|null} enfants
 */

/**
 * Ajoute enfant dans ul et à la liste d'items
 * @param {ClientItem} enfant
 * @param {HTMLElement} ul
 * @param {Object<string, ClientItemExtended[]>} items
 */
async function addChild (enfant, ul, items) {
  if (enfant.type === 'arbre') {
    const ct2 = addElement(ul, 'ul')
    return showArbre(enfant, ct2, items)
  }

  const li = addElement(ul, 'li')
  const { displayUrl } = getUrls(enfant)
  addElement(li, 'a', { href: displayUrl, target: '_blank' }, enfant.titre)
  addItem(li, enfant, items)
}

/**
 * Ajoute l'item à nos listes (et un span avec le rid à coté du titre), en affectant domElt
 * @param {HTMLElement} ct
 * @param {ClientItem} item
 * @param {Object<string, ClientItemExtended[]>} items
 */
function addItem (ct, item, items) {
  const rid = item.rid ?? item.titre
  if (item.rid) {
    addElement(ct, 'span', { className: 'rid' }, `(${rid})`)
  }
  item.domElt = ct
  if (!items[rid]) items[rid] = []
  items[rid].push(item)
}

/**
 * Retourne une version normalisée de la string (sans accents ni caractères autre que lettre/chiffre pour les comparer
 * @param {string|undefined} str
 * @return {string}
 */
function normalize (str) {
  if (typeof str !== 'string') return ''
  return toAscii(str)
    .toLowerCase() // minuscules sans accents
    .replace(/[^a-z0-9]/g, '')// sans caractères autres que a-z0-9
}

/**
 * Retourne true si item et other ont une propriété prop similaire
 * @param {string} prop La propriété à comparer, parent|previous|next ou parents qui fait du récursif sur parent
 * @param {ClientItemExtended|null} item
 * @param {ClientItemExtended|null} other
 * @return {boolean}
 */
function haveSame (prop, item, other) {
  if (item == null || other == null) return false
  if (prop === 'parents') {
    if (!haveSame('parent', item, other)) return false
    // faut du récursif sur les parents
    if (item?.parent || other?.parent) return haveSame('parents', item?.parent, other?.parent)
    // plus de parents
    return true
  }
  // on compare les rids s'ils existent, le titre sinon
  const item1 = item?.[prop]
  const item2 = other?.[prop]
  if (item1 == null && item2 == null) return true
  if (item1?.rid || item2?.rid) return item1?.rid === item2?.rid
  return normalize(item1?.titre) === normalize(item2?.titre)
}

/**
 * Retourne le "jumeau" de item dans others, si on le trouve
 * @param {ClientItemExtended} item
 * @param {ClientItemExtended[]|null} others
 * @param {number} index L'index de item dans sa liste
 * @return {ClientItemExtended|null}
 */
function getTwin (item, others, index) {
  if (!others?.length) return null
  // on cherche d'abord d'après le parent
  const candidates = others.filter(i => !i.twin && haveSame('parent', i, item))
  if (!candidates.length) return null
  if (candidates.length === 1) return candidates[0]
  // y'en a plusieurs…
  for (const candidate of candidates) {
    if (
      haveSame('previous', candidate, item) &&
      haveSame('next', candidate, item)
    ) {
      // un voisin commun, on prend celui-là
      return candidate
    }
  }
  // si on est toujours là aucun n'a les même voisins, on refait un tour en se contentant d'un seul voisin commun
  for (const candidate of candidates) {
    if (
      haveSame('previous', candidate, item) ||
      haveSame('next', candidate, item)
    ) {
      // un voisin commun, on le prend
      return candidate
    }
  }
  // toujours pas réussi à départager les candidats, on prend celui qui a le même index s'il existe, sinon le 1er
  return index < candidates.length ? candidates[index] : candidates[0]
}

/**
 * Affecte la bonne classe css à item et other, en les marquant jumeau l'un de l'autre
 * @param {ClientItemExtended} item
 * @param {ClientItemExtended|null} other
 */
function flagItems (item, other) {
  if (other) {
    // on marque la géméllité
    if (other.twin) {
      console.log('Le node', other, 'est déjà couplé, on peut pas le relier à', item)
      return
    }
    other.twin = item
    item.twin = other
    if (item.parent) addBlinkAction(item)
    // on regarde à quel point ils sont équivalents
    let className
    if (haveSame('parent', item, other)) {
      // même 1er parent
      if (haveSame('previous', item, other) && haveSame('next', item, other)) {
        // même voisins
        if (haveSame('parents', item, other)) {
          // et tous les parents identiques
          className = 'idem'
        } else {
          className = 'sameParentAndSiblings'
        }
      } else {
        className = 'sameParent'
      }
    } else {
      className = 'adopted'
    }
    item.domElt.classList.add(className)
    other.domElt.classList.add(className)
    if (className !== 'idem') {
      // faut aussi ajouter change
      item.domElt.classList.add('changed')
      other.domElt.classList.add('changed')
    }
  } else {
    // on le marque orphan
    item.twin = null
    item.domElt.classList.add('orphan')
  }
}

/**
 * Compare les éléments et masque ceux qui sont dans les deux arbres
 */
function compare () {
  // on boucle sur les items du 1er arbre
  for (const [rid, items] of Object.entries(items1)) {
    const others = items2[rid]
    // on va essayer de comparer cet item avec son "jumeau" dans l'autre arbre
    for (const [index, item] of items.entries()) {
      // on cherche l'item équivalent dans others, pas forcément au même index
      const twin = others
        ? getTwin(item, others, index)
        : null
      flagItems(item, twin)
    }
  }
  // reste à marquer orphelin tous les éléments de l'arbre2 qui n'ont pas de twin
  for (const items of Object.values(items2)) {
    for (const item of items) {
      if (!item.twin) {
        item.domElt.classList.add('orphan')
      }
    }
  }
}

function blinker (elt1, elt2) {
  let opacity = 1
  let coef = 1
  let nb = 0
  const tree1 = g('tree1')

  // on scroll la page pour mettre le ul en haut de page
  window.scrollTo(0, tree1.offsetTop)

  // on scroll chaque arbre pour mettre ce qui va clignoter au milieu
  const decal = Math.floor(tree1.offsetHeight / 2)
  tree1.scrollTo(0, elt1.offsetTop - decal)
  g('tree2').scrollTo(0, elt2.offsetTop - decal)
  const timerId = setInterval(() => {
    if (opacity >= 1) {
      coef = -1
      nb++
    } else if (opacity <= 0.3) {
      coef = 1
    }
    opacity += coef * 0.1
    if (nb > 3) {
      clearInterval(timerId)
      opacity = 1
    }
    elt1.style.opacity = opacity
    elt2.style.opacity = opacity
  }, 100)
}

function addBlinkAction (item) {
  if (!item.twin) return
  const elt1 = item.domElt
  const elt2 = item.twin.domElt
  // ajoute le picto
  const picto1 = addElement(elt1, 'span', {}, pictoStr) // y'avait aussi 👣
  // faut le remonter en 2e child
  if (elt1.childNodes.length > 2) {
    elt1.insertBefore(picto1, elt1.childNodes[1])
  }
  const picto2 = addElement(elt2, 'span', {}, pictoStr)
  if (elt2.childNodes.length > 2) {
    elt2.insertBefore(picto2, elt2.childNodes[1])
  }
  const listener = blinker.bind(null, elt1, elt2)
  picto1.addEventListener('click', listener)
  picto2.addEventListener('click', listener)
}

function changeAliasOfToRid (ref) {
  if (ref.aliasOf) {
    ref.rid = ref.aliasOf
    delete ref.aliasOf
  }
  if (ref.enfants?.length) {
    for (const enfant of ref.enfants) {
      changeAliasOfToRid(enfant)
    }
  }
}

// ******************
// Nos elts html
// ******************
const ct1 = g('tree1')
const ct2 = g('tree2')
const loading = g('loading')
const toggleButton = g('toggleCommon')
const ctErrors = g('errors')
const form = q1('form')

// comportement toggle
let isIdemDisplayed = true
toggleButton.addEventListener('click', (event) => {
  event.preventDefault() // faut pas déclencher d'event submit
  if (isIdemDisplayed) {
    // faut masquer tous les idem qui n'ont aucun enfant changed
    for (const ct of q('.idem')) {
      if (!ct.querySelector('.changed')) ct.classList.add('hidden')
    }
    toggleButton.innerText = 'Afficher les identiques'
    isIdemDisplayed = false
  } else {
    for (const ct of q('#trees .hidden')) {
      ct.classList.remove('hidden')
    }
    toggleButton.innerText = 'Masquer les identiques'
    isIdemDisplayed = true
  }
})

// img de chargement (pour le charger depuis le html il faudrait l'ajouter à la conf webpack, on l'importe en js)
loading.classList.add('hidden')
addElement(loading, 'img', { src: loadingImg })
// légende du picto
addElement(q1('.legend'), 'li', {}, `Cliquer sur ${pictoStr} pour mettre en évidence le même élément dans l’autre arbre (et les afficher l’un en face de l'autre)`)

// submit du form
form.addEventListener('submit', async (event) => {
  console.log('submit', event)
  try {
    event.preventDefault() // inutile de faire un post http
    const rid1 = g('rid1').value
    const rid2 = g('rid2').value
    if (!rid1 || !rid2) return addError('Il faut choisir deux rids', true)
    const tree1 = await fetchPublicItem(rid1)
    const tree2 = await fetchPublicItem(rid2)
    return load(tree1, tree2)
  } catch (error) {
    addError(error)
  }
})

// listener du parent qui nous envoie des comparaisons à faire
window.addEventListener('message', async ({ data: { action, src, dst } }) => {
  if (action !== 'diffArbres') return // pas pour nous
  if (!src) return addError('Il manque l’arbre source')
  if (!dst) return addError('Il manque l’arbre destination')
  console.log('comparaison de', src, 'avec', dst)
  if (typeof src === 'string') {
    src = await fetchPublicItem(src)
  } else if (src && typeof src === 'object') {
    changeAliasOfToRid(src)
  } else {
    throw Error('arbre src invalide')
  }
  if (typeof dst === 'string') {
    dst = await fetchPublicItem(dst)
  } else if (dst && typeof dst === 'object') {
    changeAliasOfToRid(dst)
  } else {
    throw Error('arbre src invalide')
  }
  return load(src, dst)
})

/* pour dev local plus rapide * /
// g('rid1').value = 'biblilocal3001/605b660685d4f6024ced4266@780'
// g('rid2').value = 'biblilocal3001/605b660685d4f6024ced4266'
g('rid1').value = 'biblilocal3001/36272'
g('rid2').value = 'biblilocal3001/36272@6'
form.querySelector('input[type=submit]').click()
/* fin ajout dev local */
