import dom from 'sesajstools/dom'
import log from 'sesajstools/utils/log'

// et pour ça faut être utilisé dans une sesathèque
import { addError } from 'client/page'

import { build } from './lib'

import imgFermer from './public/images/fermer.png'
import imgAgrandir from './public/images/agrandir.png'
import imgReduire from './public/images/reduire.png'

/**
 * Ajoute les éléments pour gérer l'aperçu et retourne une fct pour afficher une url dedans
 * @private
 * @param {HTMLElement} container
 * @return {function}
 */
function initApercu (container) {
  const $ = window.jQuery
  // un div pour les aperçus
  const attrs = {
    style: {
      position: 'absolute',
      'background-color': '#fff'
    }
  }
  const apercuContainer = dom.addElement(container, 'div', attrs)
  // en global car on s'en sert souvent, pas la peine de le recalculer dans chaque fct
  const $apercuContainer = $(apercuContainer)

  // quand on charge des swf, on a des erreurs
  // Error: Permission denied to access property 'toString'
  // que l'on peut ignorer (cf http://stackoverflow.com/a/13101119)

  dom.empty(apercuContainer)
  // en relative
  const boutons = dom.addElement(apercuContainer, 'div', {
    style: {
      position: 'absolute',
      'z-index': 2,
      float: 'right',
      right: 0
    }
  })
  const apercuFermer = dom.addElement(boutons, 'img', {
    src: imgFermer,
    alt: "fermer l'aperçu",
    style: { float: 'right' }
  })
  const apercuAgrandir = dom.addElement(boutons, 'img', {
    src: imgAgrandir,
    alt: "agrandir l'aperçu",
    style: { float: 'right' }
  })
  const apercuReduire = dom.addElement(boutons, 'img', {
    src: imgReduire,
    alt: "réduire l'aperçu",
    style: { float: 'right' }
  })
  // les actions
  const fermer = () => {
    $apercuContainer.hide()
    iframeApercu.src = undefined
    isApercu = false
  }

  const agrandir = () => {
    if (isApercu === false) {
      $apercuContainer.css('top', '5%')
      $apercuContainer.css('left', '5%')
      $apercuContainer.height('90vh')
      $apercuContainer.width('90%')
      $apercuContainer.show()
      isApercu = true
    }
  }

  const reduire = () => {
    if (isApercu) {
      $apercuContainer.height('30vh')
      $apercuContainer.width('30%')
      $apercuContainer.css('top', '65vh')
      $apercuContainer.css('left', '65%')
      isApercu = false
    }
  }

  $(apercuFermer).click(fermer)
  $(apercuAgrandir).click(agrandir)
  $(apercuReduire).click(reduire)
  // on veut pas de transparent
  $apercuContainer.css('background-color', '#fff')
  // on ajoute l'iframe dedans
  const iframeApercu = dom.addElement(apercuContainer, 'iframe', {
    style: {
      position: 'absolute',
      'z-index': 1,
      width: '100%',
      height: '100%'
    }
  })
  $apercuContainer.hide()

  // un flag pour savoir si on est en mode aperçu
  let isApercu = false

  return function show (url) {
    iframeApercu.src = url
    agrandir()
  }
}

/**
 * Affiche l'arbre, avec les boutons pour déplier les branches et afficher l'aperçu des feuilles
 * @service plugins/arbre/display
 * @param {Ressource} arbre L'arbre sous forme d'objet ressource
 * @param {object} options
 * @param {HTMLElement} options.container Le conteneur dans lequel afficher l'arbre
 * @param {HTMLElement} options.errorsContainer Le conteneur dans lequel afficher les erreurs
 * @param {errorCallback}  next       La fct à appeler quand l'arbre sera chargé (sans argument ou avec une erreur)
 */
export function display (arbre, options, next) {
  let $
  let searchInput
  let show
  if (typeof next !== 'function') {
    next = () => log('Arbre affiché')
  }

  // on lance la chaîne de promesses
  // (display est prévu pour fonctionner avec une sésathèque, on sait que jQuery n'est pas encore dans le dom)
  import('jquery').then(({ default: jQuery }) => {
    $ = jQuery
    // faut le mettre en global pour jstree
    window.jQuery = jQuery
    return import('jstree')
  }).then(() => {
    if (!arbre || arbre.type !== 'arbre') throw Error('Il faut passer un arbre à afficher')
    if (!options.container) throw new Error('Il faut passer dans les options un conteneur html pour afficher cette ressource')
    if (!options.errorsContainer) throw new Error('Il faut passer dans les options un conteneur html pour les erreurs')
    const { container } = options

    // Ajout de l'aperçu
    show = initApercu(container)
    // on crée un div pour le tree et ses compagnons
    const caseTree = dom.addElement(container, 'div')
    // la recherche
    const searchContainer = dom.addElement(caseTree, 'div', { class: 'search' })
    dom.addElement(searchContainer, 'span', null, 'Mettre en valeur les titres contenant ')
    searchInput = dom.addElement(searchContainer, 'input', { type: 'text' })
    // l'arbre
    const treeContainer = dom.addElement(caseTree, 'div')
    // les options à passer à build
    const buildOptions = {
      errorCallback: addError,
      plugins: ['search', 'contextmenu'],
      contextmenu: {
        select_node: false,
        items: function (node, cb) {
          const items = {}
          const editUrl = node.a_attr['data-editurl']
          const describeUrl = node.a_attr['data-describeurl']
          if (editUrl) {
            items.editRefBlank = {
              label: 'Éditer dans un nouvel onglet',
              action: () => window.open(editUrl, '_blank').focus()
            }
          }
          if (describeUrl) {
            items.describe = {
              label: 'Afficher la description',
              action: () => show(`${describeUrl}?layout=iframe`)
            }
            items.describeRefBlank = {
              label: 'Voir la description dans un nouvel onglet',
              action: () => window.open(describeUrl, '_blank').focus()
            }
          }
          cb(items)
        }
      }
    }
    return build(treeContainer, arbre, buildOptions)
  }).then(($tree) => {
    /* Pour récupérer un élément sous sa forme jstree, c'est (id est l'id jstree, sans #)
     * const jstNode = $.jstree.reference($tree).get_node(id)
     * et les data que l'on a mise sont dans
     * jstNode.original, par ex jstNode.original.a_attr['data-type']
     */

    // pour la recherche, on écoute la modif de l'input
    let timer
    const $searchInput = $(searchInput)
    $searchInput.keyup(function () {
      // on est appelé à chaque fois qu'une touche est relachée dans cette zone de saisie
      // on lancera la recherche dans 1/4s si y'a pas eu d'autre touche
      if (timer) {
        clearTimeout(timer)
      }
      timer = setTimeout(function () {
        const v = $searchInput.val()
        $tree.jstree(true).search(v)
      }, 250)
    })

    // pour l'aperçu, on peut pas écouter les clic sur a.jstree-anchor ni li.jstree-node car jstree les intercepte
    // on écoute donc l'événement select sur le jstree
    $tree.on('select_node.jstree', function (e, data) {
      const jstNode = data.node.original
      log("on veut l'aperçu du node", jstNode, data)
      if (jstNode && jstNode.a_attr) {
        if (jstNode.a_attr['data-type'] === 'arbre') {
          // on fait du toggle
          log('toggle sur l’arbre ' + jstNode.text)
          if ($tree.jstree('is_open', data.node)) $tree.jstree('close_node', data.node)
          else $tree.jstree('open_node', data.node)
        } else {
          // on lance l'aperçu de ce node
          log(`on va charger en aperçu ${jstNode.a_attr.href}`)
          show(jstNode.a_attr.href)
        }
      }
    })
  }).then(next).catch(addError)
} // display
