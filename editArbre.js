import { addElement } from 'sesajstools/dom'
import log from 'sesajstools/utils/log'
import $ from 'jquery'
import { addSesatheques, exists, fetchPublicRef } from 'sesatheque-client/src/fetch'
import { addNode, build, getEnfants, getPos } from './lib'
// ça vient de sesatheque
import { sesatheques } from 'server/config'

// la css jstree
import 'jstree/dist/themes/default/style.css'
// une css de la sesatheque (c'est son webpack qui nous compile)
import 'client-react/styles/display.scss'
// et la notre
import './public/arbre.css'

import iconArbre from './public/images/arbre.gif'

// on active le debug si c'est demandé, mais on peut pas utiliser getParameter car on veut regarder l'url du parent
// (on est dans l'iframe edit.html)
try {
  if (/\?(.+&)?debug=(all|edit)/.test(window.parent.location)) {
    log.setLogLevel(log.levels.debug)
    log.debug('Log de débug activé (edit)')
  }
} catch (error) {
  console.error(error)
}

addSesatheques(sesatheques)

/**
 * @callback loadCallback
 * @param {Error} [error]
 * @param {function} getParametres Fonction qui doit renvoyer une string json des parametres, pour un arbre un array des enfants
 */
/**
 * @callback changeCallback
 * @param {string} jsonString
 */
/**
 * Édite un arbre (avec jstree, src et dst), appelé depuis la vue editArbre depuis l'url /ressource/modifier/xxx
 * @service plugins/arbre/edit
 * @param arbre
 * @param {object} options
 * @param {string} options.baseId
 * @param {HTMLElement} [options.container]
 * @param {object[]} [options.sesatheques] d'éventuelles sésathèques (obligatoire si on connait pas les baseId des ref qu'on va nous demander de charger)
 * @param {loadCallback} [options.loadCallback]
 * @param {changeCallback} [options.changeCallback]
 */
export default function editArbre (arbre, { baseId, container, sesatheques, loadCallback, changeCallback } = {}) {
  // nos fcts internes
  /**
   * Ajoute le lien aperçu aux links
   * @param {object} links l'objet contextmenu.items de jstree
   * @param node
   */
  function addLinkApercu (links, node) {
    const url = node.a_attr && node.a_attr.href
    // Apercu sur tous les éléments dont on a une ref
    if (!url) return
    links.apercu = {
      label: 'Aperçu ci-dessous',
      action: () => { iframeApercu.src = url },
      icon: 'jstIconApercu'
    }
  }

  /**
   * Ajoute le lien d'aperçu externe aux links
   * @param {object} links l'objet contextmenu.items de jstree
   * @param node
   */
  function addLinkVoirBlank (links, node) {
    const url = node.a_attr && node.a_attr.href
    if (!url) return

    links.voirBlank = {
      label: 'Voir dans un nouvel onglet',
      action: () => window.open(url, '_blank').focus(),
      icon: 'jstIconVoirBlank'
    }
    links.decrireBlank = {
      label: 'Décrire dans un nouvel onglet',
      action: () => window.open(url.replace('public/voir', 'ressource/decrire'), '_blank').focus(),
      // raccourci F4 marche pas…
      // shortcut: 115, // cf http://keycode.info/ ou https://www.toptal.com/developers/keycode
      // shortcut_label: 'F4',
      icon: 'jstIconDecrireBlank'
    }
  }

  /**
   * Affiche un message d'erreur sur la page mais près de l'arbre en cours d'édition
   * @private
   * @param {string} error
   * @param {number} [delay] Si fourni efface le message après ce nb de s ou ms (si <1000 on * par 1000)
   */
  function addTreeError (error, delay) {
    let errorMessage
    if (typeof error === 'string') {
      errorMessage = error
      error = Error(error)
    } else {
      errorMessage = error.toString()
    }
    console.error(error)
    $treeError.text(errorMessage)
    $treeError.show()
    if (delay) {
      if (delay < 1000) delay *= 1000 // on nous a passé des s
      setTimeout(
        () => {
          $treeError.empty()
          $treeError.hide()
        }, delay
      )
    }
  }

  /**
   * Ajoute le div pour l'arbre source et la gestion de son chargement
   * @private
   */
  function addLoadSrc (parent) {
    const labelArbreSource = addElement(parent, 'label', { for: 'loadRef' }, 'Arbre source')
    inputRef = addElement(labelArbreSource, 'input', { id: 'loadRef', type: 'text', style: { margin: '0 1em' } })
    // lien de chargement
    const loadButton = addElement(labelArbreSource, 'button', {}, 'afficher')

    addElement(labelArbreSource, 'br')
    const rmqProps = { class: 'rmq', style: { 'max-width': '50rem' } }
    addElement(labelArbreSource, 'span', rmqProps, 'Vous pouvez indiquer un id seul ou un rid de la forme baseId/id (pour le charger sur une autre sesatheque), et vous pouvez préciser la version en ajoutant @xx à la fin)')
    addElement(labelArbreSource, 'br')
    addElement(labelArbreSource, 'span', rmqProps, 'Destiné à y prendre des éléments pour les glisser vers l’arbre à modifier.')

    // pour charger l'arbre qui contient tous les autres @todo le mettre en configuration
    addElement(labelArbreSource, 'br')
    const loadBigParent = () => {
      inputRef.value = 'sesabibli/50035'
      loadButton.click()
    }
    const aProps = { style: { fontSize: '0.9em' } }
    addElement(labelArbreSource, 'a', aProps, 'Arbre général').onclick = loadBigParent
    if (arbre.version > 1) {
      addElement(labelArbreSource, 'br')
      const a = addElement(labelArbreSource, 'a', aProps, 'Version précédente de l’arbre à modifier')
      a.addEventListener('click', () => {
        inputRef.value = `${arbre.rid}@${arbre.version - 1}`
        loadButton.click()
      })
    }

    $inputRef = $(inputRef)
    // enter doit pas valider le form mais charger la ref
    $inputRef.keypress(function (event) {
      if (event.keyCode === 13) {
        loadSrc()
        event.preventDefault() // pour empêcher le submit
      }
    })
    $(loadButton).click(loadSrc)
  }

  /**
   * Crée un json de la liste des enfants de l'arbre destination
   * @private
   * @return {string} Le json de l'arbre destination (ou string vide si y'a eu un pb, signalé par ailleurs)
   */
  function getJSON () {
    let enfants
    let enfantsStr = '[]'
    try {
      if (!$dstTree || typeof $dstTree.jstree !== 'function') {
        return addTreeError('Impossible de récupérer l’instance de l’arbre')
      }
      const inst = $dstTree.jstree(true)
      enfants = getEnfants(inst)
      log.debug('getJSON récupère les enfants', enfants)
      // on a les enfants de la racine, il ne doit y en avoir qu'un
      if (enfants.length !== 1) {
        addTreeError('Il ne doit y avoir qu’une racine')
        return ''
      }
      if (enfants[0].enfants && enfants[0].enfants.length) {
        // ce que l'on retourne sera affiché dans le mode texte de l'éditeur, on formate
        enfantsStr = JSON.stringify(enfants[0].enfants, null, 2)
      }
    } catch (error) {
      log.error('Le parsing json a planté', error, enfants)
      return addTreeError('Erreur interne, impossible de récupérer les enfants')
    }

    return enfantsStr
  }

  /**
   * Initialise les éléments du dom et nos variables internes
   * @private
   */
  function initDom () {
    if (!container) container = document.getElementById('display')

    // un p pour les erreurs
    const treeError = addElement(container, 'p', { class: 'error' })
    $treeError = $(treeError)
    $treeError.hide()

    // arbre source
    srcGroup = addElement(container, 'div', { id: 'srcGroup' })
    addLoadSrc(srcGroup)
    // la recherche
    const searchContainer = addElement(srcGroup, 'div', { class: 'search', style: { marginTop: '0.3em' } })
    const searchInputLabel = addElement(searchContainer, 'label', { style: { fontWeight: 'normal' } }, 'Mettre en valeur les titres contenant ')
    srcSearchInput = addElement(searchInputLabel, 'input', { type: 'text' })
    divSrcTree = addElement(srcGroup, 'div')

    // arbre destination (qu'on édite)
    const dstGroup = addElement(container, 'div', { id: 'dstGroup' })
    let div = addElement(dstGroup, 'div')
    addElement(div, 'strong', null, 'arbre à modifier')
    addElement(div, 'em', { style: { 'font-size': '0.8em' } }, ' (clic droit pour enlever des éléments ou ajouter des dossiers)')
    const dstSearchInputLabel = addElement(dstGroup, 'label', { style: { display: 'block', fontWeight: 'normal' } }, 'Mettre en valeur les titres contenant ')
    dstSearchInput = addElement(dstSearchInputLabel, 'input', { type: 'text' })
    divDstTree = addElement(dstGroup, 'div')
    // diff des modifs en cours
    let button = addElement(dstGroup, 'button', {}, 'voir les modifications qui seraient enregistrées')
    button.addEventListener('click', () => deferDiff(arbre))

    div = addElement(container, 'div', { style: { clear: 'both' } })
    // lien diff src / dst
    button = addElement(div, 'button', {}, 'voir les différences entre les deux arbres ci-dessus')
    button.addEventListener('click', () => {
      if (!$srcTree) return console.error(Error('Pas d’arbre source à comparer'))
      const _arbre = getEnfants($srcTree.jstree(true))[0]
      _arbre.aliasOf = arbre.rid
      deferDiff(_arbre)
    })
    // un lien vers l'outil de diff à part
    addElement(div, 'a', { href: 'diffArbres.html', target: '_blank', style: { marginLeft: '2rem' } }, 'Outil générique de visualisation de différences d’arbre (nouvel onglet)')

    // aperçu
    addElement(container, 'p', { style: 'clear:both;' }, "Aperçu d'un élément")
    iframeApercu = addElement(container, 'iframe', { id: 'apercu' })
  }

  /**
   * Listener appelé au clic sur un lien de diff
   * @param _arbre
   */
  function deferDiff (_arbre) {
    if (/diffArbres.html$/.test(iframeApercu.src)) {
      runDiff(_arbre)
    } else {
      iframeApercu.onload = () => runDiff(_arbre)
      iframeApercu.src = 'diffArbres.html'
    }
  }

  /**
   * Appelé par deferDiff (directement ou au onLoad de l'iframe)
   * @param _arbre
   */
  function runDiff (_arbre) {
    if (!_arbre) console.error(Error('arbre manquant'))
    const data = {
      action: 'diffArbres',
      src: _arbre,
      dst: getEnfants($dstTree.jstree(true))[0]
    }
    iframeApercu.contentWindow.postMessage(data, '*')
  } /* */

  /**
   * Charge l'arbre destination
   * @private
   * @param arbre
   */
  function loadDst (arbre) {
    // nos cb au clic droit

    // ajoute une ressource
    function actionAdd (data) {
      let id = window.prompt('Id de la ressource\nPréfixe “sesabibli/” ou “sesacommun/” possible pour préciser la sesathèque à utiliser')
      if (id) {
        if (!id.includes('/')) id = baseId + '/' + id
        addNode($dstTree, id, { parentRef: data.reference }, function (error/* , newNode */) {
          if (error) addTreeError(error)
          else fwChanges()
        })
      }
    }

    // Cb sur clic droit ajouter un dossier
    function actionAddFolder (data) {
      // la cb appellée avec le node créé
      function createCb (newNode) {
        // ici inst.edit existe bien, mais si c'est le 1er enfant d'un arbre vide
        // log.debug('dans createCb', newNode, inst)
        inst.edit(
          newNode,
          'titre',
          function (newNode, status) {
            if (status) fwChanges()
            else console.error(Error('L’ajout de dossier ne retourne pas de status ok'), status)
            log.debug('après modif', inst)
          }
        )
      }
      // en 3.3 ce truc marche plus
      // const inst = $.jstree.reference(data.reference)
      // on fait plutôt
      const inst = $dstTree.jstree(true)
      const parentNode = inst.get_node(data.reference)
      const newNode = {
        icon: iconArbre,
        a_attr: { 'data-type': 'arbre' }
      }
      inst.create_node(parentNode, newNode, 'last', createCb)
    } // actionAddFolder

    // retirer un item de l'arbre
    function actionDelete (data) {
      if (confirm('Êtes-vous sûr de vouloir supprimer cet élément ?')) {
        const inst = $dstTree.jstree(true)
        const node = inst.get_node(data.reference)
        if (inst.is_selected(node)) inst.delete_node(inst.get_selected())
        else inst.delete_node(node)
        fwChanges()
      }
    }

    // Aller éditer la ressource dans un autre onglet
    function actionEdit (data) {
      const inst = $dstTree.jstree(true)
      const node = inst.get_node(data.reference)
      const url = node.a_attr['data-editurl']
      if (!url) return addTreeError('Url d’édition manquante')
      window.open(url, '_blank').focus()
    }

    // rafraîchir les infos de cette ressource
    function actionRefresh (data) {
      const inst = $dstTree.jstree(true)
      const node = inst.get_node(data.reference)
      const aliasOf = node.a_attr['data-aliasof']
      if (!aliasOf) return addTreeError('aliasOf manquant')
      let pos = getPos(inst, node)
      if (pos === -1) {
        console.error(Error(`Pas trouvé la position du node (${node.id} : ${node.text})`))
        pos = 'last'
      }
      addNode($dstTree, aliasOf, { parentRef: node.parent, pos }, function (error/* , newNode */) {
        if (error) return addTreeError(error)
        // le nouveau est ajouté, on peut détruire l'ancien
        inst.delete_node(node)
        fwChanges()
      })
    }

    // Renommer un dossier
    function actionRename (data) {
      const inst = $dstTree.jstree(true)
      const node = inst.get_node(data.reference)
      inst.edit(node, null, (newNode, status) => {
        if (status) fwChanges()
        else console.error(Error('Le renommage a échoué'))
      })
    }

    // Main de loadDst
    const buildOptions = {
      noExpandExternal: true,
      check_callback: function (action, node, parent) {
        log.debug('check_callback avec', arguments)
        // on accepte le drop au 1er niveau
        if (parent.parent === '#') return true
        // et dans des arbres (dossiers)
        if (['move_node', 'copy_node'].includes(action)) {
          return (
            // pas sur la racine elle-même qui ne doit garder qu'un seul enfant, l'arbre en cours d'édition
            parent.id !== '#' &&
            // un arbre
            parent.a_attr && parent.a_attr['data-type'] === 'arbre' &&
            // qui n'est pas une ref externe
            !parent.a_attr['data-aliasof']
          )
        }
        // tout le reste est autorisé
        return true
      },
      plugins: ['dnd', 'contextmenu', 'search'],
      contextmenu: {
        select_node: false,
        // on met une fct car le résultat dépend de l'item sur lequel on fait un clic droit
        // cf $jstree.defaults.contextmenu sur https://github.com/vakata/jstree/blob/master/src/jstree.contextmenu.js#L58
        /**
         * La liste de nos éléments de menu
         * @see http://www.jstree.com/api/#/?q=$jstree.defaults&f=$jstree.defaults.contextmenu.items
         * @private
         * @param node Le node, avec les propriétés a_attr, icon, text
         * @param cb à rappeler avec les items du menu contextuel pour ce node
         */
        items: function (node, cb) {
          const items = {}
          const isRacine = (node.parent === '#')
          const isArbre = node.a_attr['data-type'] === 'arbre'
          const hasRef = Boolean(node.a_attr['data-aliasof'])
          const isArbreSansRef = isArbre && !hasRef

          // On peut créer dans la racine ou les éléments arbre qui ne sont pas une ref vers un autre arbre (sinon faut aller éditer l'original)
          if (isRacine || isArbreSansRef) {
            // idem pour les ressources
            items.add = {
              label: 'Ajouter une ressource',
              action: actionAdd,
              icon: 'jstIconAdd'
            }
            items.create = {
              label: 'Ajouter un dossier',
              action: actionAddFolder,
              icon: 'jstIconAddFolder'
            }
          }
          // à la racine c'est tout
          if (isRacine) return cb(items)

          if (isArbreSansRef) {
            // on peut renommer les arbres sans ref
            items.rename = {
              label: 'Renommer (F2)',
              action: actionRename,
              shortcut: 113, // cf http://keycode.info/ ou https://www.toptal.com/developers/keycode
              shortcut_label: 'F2',
              icon: 'jstIconRename'
            }
          }
          if (hasRef) {
            // Voir dans un nouvel onglet (v)
            addLinkVoirBlank(items, node)

            // un raccourci pour aller éditer une ref
            const editUrl = node.a_attr['data-editurl']
            if (editUrl) {
              items.editRefBlank = {
                label: 'Éditer dans un nouvel onglet',
                action: actionEdit,
                icon: 'jstIconEdit'
              }
            }

            const dataUrl = node.a_attr['data-dataurl']
            if (dataUrl) {
              items.refresh = {
                label: 'Rafraîchir',
                action: actionRefresh,
                icon: 'jstIconRefresh'
              }
            } else {
              console.error(Error('node sans attribut data-url'), node)
            }

            // Aperçu ci-dessous (a)
            addLinkApercu(items, node)
          }

          // on peut supprimer n'importe quel item (sauf la racine)
          items.remove = {
            label: 'Supprimer',
            action: actionDelete,
            icon: 'jstIconRemove'
          }

          log.debug('clic droit sur', node)
          cb(items)
        }
      },
      dnd: {
        inside_pos: 'last'
      },
      listeners: {
        'move_node.jstree': fwChanges, // drop from arbre dst
        'copy_node.jstree': fwChanges, // drop from arbre src
        'select_node.jstree': function (e, data) {
          const jstNode = data.node.original
          log.debug('clic sur', jstNode)
          if (jstNode && jstNode.a_attr && jstNode.a_attr['data-type'] === 'arbre' && !jstNode.a_attr['data-aliasof']) {
            // on fait du toggle
            if ($dstTree.jstree('is_open', data.node)) $dstTree.jstree('close_node', data.node)
            else $dstTree.jstree('open_node', data.node)
          }
        }
      }
    } // buildOptions

    build(divDstTree, arbre, buildOptions).then($tree => {
      if (!$tree.jstree) throw Error('jstree n’a pas été chargé correctement, impossible d’afficher des arbres')
      $dstTree = $tree
      // on ajoute le comportement search
      let timer
      const $dstSearchInput = $(dstSearchInput)
      const listener = () => {
        // on est appelé à chaque fois qu'une touche est relachée dans cette zone de saisie
        // on lancera la recherche dans 1/4s si y'a pas eu d'autre touche
        if (timer) clearTimeout(timer)
        timer = setTimeout(function () {
          const v = $dstSearchInput.val()
          $dstTree.jstree(true).search(v)
        }, 250)
      }
      $dstSearchInput.change(listener)
      $dstSearchInput.keyup(listener)
    }).catch(addTreeError)
  } // loadDst

  /**
   * Charge l'arbre source d'après le contenu de $inputRef
   * @private
   * @return {boolean}
   */
  function loadSrc () {
    const rid = $inputRef.val()
    log.debug('On va charger en source ' + rid)
    if (rid) {
      const slashPos = rid.indexOf('/')
      if (slashPos === -1) {
        fetchPublicRef(baseId, rid, showSrc)
      } else {
        const debut = rid.substring(0, slashPos)
        if (exists(debut)) {
          fetchPublicRef(debut, rid.substring(slashPos + 1), showSrc)
        } else {
          fetchPublicRef(baseId, rid, showSrc)
        }
      }
    } else {
      log.error(Error('appel de load sans rid'))
    }
  } // loadSrc

  /**
   * Affiche l'arbre en src
   * @param error
   * @param {Ref} arbre
   * @return {*}
   */
  function showSrc (error, arbre) {
    const rid = $inputRef.val()
    if (error) return addTreeError(`Erreur au chargement de ${rid} : ${error}`)
    if (!arbre) return addTreeError(`L’arbre ${rid} n’existe pas`)
    if (arbre.type !== 'arbre') return addTreeError(`La ressource ${rid} n’est pas un arbre`)
    // on peut y aller
    const buildOptions = {
      plugins: ['contextmenu', 'dnd', 'search'],
      contextmenu: {
        items: function (node, cb) {
          // cf http://www.jstree.com/api/#/?q=$jstree.defaults&f=$jstree.defaults.contextmenu.items
          const links = {}
          addLinkApercu(links, node)
          addLinkVoirBlank(links, node)

          // ajout du 'charger ici'
          const aliasOf = node.a_attr && node.a_attr['data-aliasof']
          if (aliasOf && node.a_attr['data-type'] === 'arbre') {
            links.replace = {
              label: 'Charger ici',
              action: function () {
                log.debug('Charger en source ' + aliasOf)
                $inputRef.val(aliasOf)
                loadSrc()
              },
              icon: 'jstIconReplace'
            }
          }
          // log.debug('clic droit dans la source sur', node)
          cb(links)
        },
        // on veut pas qu'un clic droit sélectionne le node
        // cf https://www.jstree.com/api/#/?f=$.jstree.defaults.contextmenu.select_node
        select_node: false
      },
      dnd: {
        always_copy: true
      },
      listeners: {
        // pour ouvrir / fermer, on peut pas écouter les clic sur a.jstree-anchor ni li.jstree-node
        // car jstree les intercepte, on écoute donc l'événement select_node
        // cf https://www.jstree.com/api/#/?f=select_node.jstree
        'select_node.jstree': function (e, data) {
          const jstNode = data.node.original
          log.debug('arbre src, clic sur', jstNode)
          if (jstNode && jstNode.a_attr && jstNode.a_attr['data-type'] === 'arbre') {
            // on fait du toggle
            if ($srcTree.jstree('is_open', data.node)) $srcTree.jstree('close_node', data.node)
            else $srcTree.jstree('open_node', data.node)
          }
        }
      }
    }
    build(divSrcTree, arbre, buildOptions).then($tree => {
      $srcTree = $tree
      // pour la recherche, on remet le comportement de la modif de l'input sur l'arbre, sur keyup + change
      let timer
      const $srcSearchInput = $(srcSearchInput)
      const listener = () => {
        // on est appelé à chaque fois qu'une touche est relachée dans cette zone de saisie
        // on lancera la recherche dans 1/4s si y'a pas eu d'autre touche
        if (timer) clearTimeout(timer)
        timer = setTimeout(function () {
          const v = $srcSearchInput.val()
          $srcTree.jstree(true).search(v)
        }, 250)
      }
      $srcSearchInput.change(listener)
      $srcSearchInput.keyup(listener)

      // on décale de la hauteur du label du bloc source, pour que ce soit aligné
      const jstSrc = document.querySelector('#srcGroup .jstree')
      const jstDst = document.querySelector('#dstGroup .jstree')
      if (jstSrc && jstDst) {
        jstDst.style.marginTop = 0 // faut remettre à 0 avant de refaire le calcul
        jstDst.style.marginTop = `${jstSrc.offsetTop - jstDst.offsetTop}px`
      } else {
        console.error(Error('pas trouvé "#srcGroup > .jstree" ou "#dstGroup > .jstree"'), jstSrc, jstDst)
      }
    }).catch(addTreeError)
  } // showSrc

  // ###########
  // MAIN
  // ###########
  if (!baseId) throw new Error('Erreur interne, paramètre baseId manquant')
  if (sesatheques) addSesatheques(sesatheques)

  // la cb pour propager les changements
  const fwChanges = () => {
    if (!changeCallback) return
    const json = getJSON()
    // si ça plante faut surtout rien propager (un clic sur enregistrer conservera la version précédente,
    // sinon on risque de vider l'arbre)
    if (!json) return
    changeCallback(json)
  }

  // les containers (variables locales au module), qui seront affectés par initDom()
  let iframeApercu, srcGroup, inputRef, srcSearchInput, dstSearchInput, divSrcTree, divDstTree
  // quasi les mêmes jquerifiée
  let $inputRef, $treeError
  // les arbres avec .jstree après le build
  let $srcTree, $dstTree

  initDom()
  // on charge l'arbre à éditer
  loadDst(arbre)
  // on propage la fct pour récupérer le json si on nous la demande
  if (loadCallback) loadCallback(null, getJSON)
}

// répercuter les modifs dans sesatheque:app/_typedef.doc.js
/**
 * Un enfant d'un arbre
 * @typedef Enfant
 * @property {string} titre
 * @property {string} type
 * @property {boolean} public
 * @property {string} [aliasOf] Absent seulement pour les "dossiers" (ressources de type arbre sans aliasOf mais avec enfants)
 * @property {string} [resume]
 * @property {string} [commentaires]
 * @property {Enfant[]} [enfants]
 */
