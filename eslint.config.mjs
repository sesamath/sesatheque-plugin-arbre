// Cf https://eslint.org/docs/latest/use/configure/
// et https://eslint.org/docs/latest/use/configure/migration-guide

import globals from 'globals'
import neostandard from 'neostandard'
import react from 'eslint-plugin-react'

export default [
  {
    settings: {
      react: {
        version: '16.6.3',
      }
    }
  },
  // la conf standard, cf https://github.com/neostandard/neostandard?tab=readme-ov-file#configuration-options
  ...neostandard({
    globals: {
      ...globals.browser,
    },
  }),

  // react
  react.configs.flat.recommended,

  // et nos ajouts
  {
    files: ['**/*.{js,ts}'],
    rules: {
      // un alert() doit lancer une erreur
      'no-alert': 'error',
      // et un console.log aussi (on autorise console.error, warn, …)
      'no-console': ['error', { allow: ['error', 'warn', 'info', 'debug'] }]
    }
  }
]
