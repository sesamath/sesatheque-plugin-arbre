import getUrls from 'sesatheque-client/src/getUrls'
import sesajstools from 'sesajstools'
import log from 'sesajstools/utils/log'
import iconArbre from '../public/images/arbre.gif'
import iconArbreRef from '../public/images/arbreRef.gif'
import { getEnfants } from './index'

// les css pour toutes les classes ${type}JstNode
import '../public/icons.css'

const { hasProp } = sesajstools
const icons = {
  arbre: iconArbre
}
// on avait tenté de mettre un `import('plugins/icons')` dans un
// `if (getBaseIdFromUrlQcq(window.location, false))`
// pour ne charger ça que si on était compilé dans une sésathèque,
// mais webpack teste quand même cette résolution et plante si ça n'existe pas (hors sésathèque donc)
// on charge donc la css par défaut, et une sesathèque pourra appeler setIcons si elle préfère
// passer les icônes que déclarent ses plugins.

/**
 * @typedef NodeAttrs
 * @property {string} alt
 * @property {string} data-type
 * @property {string} [data-aliasof]
 * @property {string} [data-dataurl]
 * @property {string} [data-describeurl]
 * @property {string} [data-editurl]
 * @property {string} [href]
 */

/**
 * Retourne les datas qui nous intéressent à mettre sur le tag a
 * (propriété a_attr du node
 * @private
 * @param {Ressource|Ref} ressource
 * @return {NodeAttrs}
 */
function getAttrs (ressource) {
  // est-ce bien une ressource (un dossier d'arbre sans aliasOf ni rid est possible)
  const originalRid = ressource.aliasOf || ressource.rid
  if (originalRid) {
    const props = getUrls(ressource)
    const attrs = {
      alt: ressource.resume || '',
      'data-type': ressource.type,
      'data-aliasof': originalRid,
      'data-dataurl': props.dataUrl,
      'data-describeurl': props.describeUrl,
      'data-editurl': props.editUrl,
      href: props.displayUrl
    }
    if (ressource.commentaires) attrs['data-commentaires'] = ressource.commentaires
    if (ressource.categories) attrs['data-categories'] = JSON.stringify(ressource.categories)
    // public
    if (hasProp(ressource, 'public')) {
      attrs['data-public'] = ressource.public ? '1' : '0'
    } else if (hasProp(ressource, 'restriction')) {
      if (hasProp(ressource, 'publie') && !ressource.publie) attrs['data-public'] = '0'
      else attrs['data-public'] = ressource.restriction ? '0' : '1'
    }
    return attrs
  }
  if (ressource.type === 'arbre') {
    // c'est un dossier sans rid ni aliasOf
    return {
      alt: '',
      'data-type': 'arbre'
    }
  }

  return {
    alt: 'Élément invalide',
    'data-type': 'error'
  }
}

/**
 * Retourne un node jstree (propriétés text, icon et a_attr qui porte nos data)
 * SANS les enfants (utiliser toJstree pour les avoir, il combine getJstNode et getJstreeChildren)
 * @see http://www.jstree.com/docs/json/ pour le format
 * @private
 * @param {Ressource|Ref} ressource
 */
export function getJstNode (ressource) {
  if (!ressource) throw Error('getJstNode appelé sans ressource')
  const { type } = ressource
  let icon
  // picto alias si c'est un arbre externe
  if (type === 'arbre') icon = ressource.aliasOf ? iconArbreRef : iconArbre
  else icon = icons[type] ? icons[type] : `${type}JstNode`

  return {
    text: ressource.titre,
    a_attr: getAttrs(ressource),
    icon
  }
}

/**
 * Retourne un tableau children au format jstree
 * @param {Ressource} ressource
 * @param {boolean} [noExpandExternal=false] passer true pour empêcher de déplier les arbres externes
 * @return {Array} Le tableau des enfants
 */
export function getJstreeChildren (ressource, noExpandExternal = false) {
  if (!ressource) throw Error('Ressource invalide')
  if (ressource.type !== 'arbre') throw Error('la ressource n’est pas un arbre')
  // ref d'un arbre (il faudra un appel ajax pour aller chercher les enfants)
  if (ressource.aliasOf) return !noExpandExternal
  // enfants vides
  if (!ressource.enfants || !ressource.enfants.length) return []
  // faut formater les enfants
  return ressource.enfants.map(enfant => {
    // on veut pas qu'un enfant foireux plante tout l'arbre
    try {
      // on propage la lecture seule si c'est le cas
      if (ressource._droits === 'R' && !hasProp(enfant, '_droits')) enfant._droits = 'R'
      // ressource non arbre, on rend
      if (enfant.type !== 'arbre') return getJstNode(enfant)
      // arbre externe que l'on ne veut pas rendre
      if (enfant.aliasOf && noExpandExternal) return getJstNode(enfant)
      // sinon on continue en récursif
      return toJstree(enfant, noExpandExternal)
    } catch (error) {
      console.error(error)
      return {
        alt: (enfant && enfant.titre) || 'enfant invalide',
        text: error.toString(),
        a_attr: {
          'data-type': 'error'
        },
        icon: 'errorJstNode'
      }
    }
  })
}

/**
 * Ajoute / met à jour une icone
 * @param {string} type le type de plugin
 * @param {string} icon une classe css ou une image (retournée par `import from 'fichier.png'`)
 */
export function setIcon (type, icon) {
  icons[type] = icon
}

/**
 * Transforme une ressource de la bibli en node pour jstree
 * (il faudra le mettre dans un tableau, à un seul élément si c'est un arbre)
 * @param {Ressource|Ref} ressource Une ressource ou une référence à une ressource
 * @param {boolean} [noExpandExternal=false] passer true pour empêcher de déplier les arbres externes
 * @returns {Object}
 */
export function toJstree (ressource, noExpandExternal = false) {
  const node = getJstNode(ressource)
  if (ressource.type === 'arbre') {
    node.children = getJstreeChildren(ressource, noExpandExternal)
    // icone ref pour les arbres en ref externe
    if (ressource.aliasOf) node.icon = iconArbreRef
  }

  return node
}

/**
 * Retourne objet Enfant à mettre dans le json
 * (avec seulement titre, type, aliasOf, enfants)
 * @param {Object} jstree
 * @param {string} childId
 * @returns {Enfant}
 */
export function toEnfant (jstree, childId) {
  const enfant = {}
  const child = jstree.get_node(childId)
  if (!child) throw Error(`Élément ${childId} introuvable`)
  if (child.text && child.a_attr && child.a_attr['data-type']) {
    // on a le minimum requis
    enfant.titre = child.text
    enfant.type = child.a_attr['data-type']
    if (child.a_attr['data-aliasof']) enfant.aliasOf = child.a_attr['data-aliasof']
    if (child.a_attr['data-commentaires']) enfant.commentaires = child.a_attr['data-commentaires']
    if (child.alt) enfant.resume = child.alt
    if (child.children && child.children.length) enfant.enfants = getEnfants(jstree, childId)
    enfant.public = hasProp(child.a_attr, 'data-public') ? child.a_attr['data-public'] === '1' : true
  } else {
    log.error('node impossible à convertir en Enfant', child)
    throw Error('élément invalide')
  }

  return enfant
}
