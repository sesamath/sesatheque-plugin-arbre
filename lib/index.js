// ce module est réservé à un usage dans un navigateur (build utilise jQuery)
// les enfants des arbres étaient des Ref[] en v2, ils deviennent des Enfant[] en v3 (2024-04-18)
// avec seulement { titre, type, aliasOf?, enfants? }

import { fetchRef, callApiUrl } from 'sesatheque-client/src/fetch'
import log from 'sesajstools/utils/log'

import { getJstreeChildren, toEnfant, toJstree } from './convert'

import 'jstree/dist/themes/default/style.min.css'
import '../public/arbre.css'

/**
 * Une mémorisation des conteneurs associés à des éléments pour build
 * @private
 * @type {Map}
 */
const builded = new Map()

/**
 * Retourne une callback à mettre dans un arbre jstree dans core.data
 * @private
 * @param {Ressource} arbre ressource de type arbre (la racine)
 * @param {object} options avec éventuellement timeout et errorCallback
 * @returns {Function} fct à mettre sur jstree.core.data, appelée avec (node, cb) qui doit rappeler cb avec la liste des enfants
 *                     cf https://www.jstree.com/api/#/?q=data&f=$.jstree.defaults.core.data
 */
function getDataCallback (arbre, options = {}) {
  /**
   * Helper de dataCallback pour les erreurs (log + envoie à l'éventuelle options.errorCallback + next)
   * @private
   * @param {Error} error
   * @param {function} next la fct next de dataCallback (qui attend un array et ne gère pas les erreurs)
   */
  function errorCallback (error, next) {
    if (typeof options.errorCallback === 'function') options.errorCallback(error)
    // et de toute façon on log et rappelle next
    log.error(error)
    // on appelle next pour afficher l'erreur dans l'arbre (permet de voir où est le pb)
    const jstErrorElt = {
      text: error.toString(),
      icon: 'errorJstNode'
    }
    next([jstErrorElt])
  }

  /**
   * Callback à mettre dans core.data de l'objet passé à jstree pour initialiser un arbre
   * @private
   * @param {object} node node jstree
   * @param {function} next callback à appeler avec les enfants à charger (donc un array)
   */
  function dataCallback (node, next) {
    log.debug('dataCallback sur le node', node)
    if (!node) return errorCallback(Error('Demande de chargement sans node à charger'), next)
    if (node.id === '#') {
      const rootElt = toJstree(arbre, noExpandExternal)
      rootElt.state = { opened: true }
      log.debug('rootNode', rootElt)
      return next(rootElt)
    }
    // si c'est un arbre et qu'on veut pas charger les arbres externes, on retourne un array vide
    // attention, passer null plante jstree, passer false laisse le triangle pour déplier tout le temps
    // avec [] ce triangle disparaît au 1er clic dessus…
    if (noExpandExternal && node.a_attr['data-type'] === 'arbre' && node.a_attr['data-aliasof']) return next([])

    // si y'à déja les children rien à faire sinon les retourner,
    // mais normalement jstree nous appelle pas dans ce cas là
    if (node.children && node.children.length) return next(node.children)

    // faut faire l'appel ajax nous même car jstree peut pas mixer json initial + ajax ensuite
    // @see http://git.net/jstree/msg12107.html
    const url = node.a_attr && node.a_attr['data-dataurl']
    if (url) {
      callApiUrl(url, {}, function (error, ressource) {
        // on a toujours error ou ressource non vide sans propriété error
        if (error) return errorCallback(error, next)
        if (ressource.type === 'arbre') {
          const children = getJstreeChildren(ressource)
          log.debug('children récupérés', children)
          return next(children)
        }
        log.error(Error('le chargement des enfants ne remonte pas un arbre'))
        log.error('le node', node)
        log.error('la ressource récupérée', ressource)
        errorCallback(Error(`Aucun enfant récupéré sur ${url}`), next)
      })
    } else {
      errorCallback(Error('Pas d’url sur le node pour récupérer ses éléments'), next)
    }
  }

  if (!arbre) throw Error('arbre manquant')
  if (arbre.type !== 'arbre') throw Error('La ressource n’est pas un arbre')
  if (!options) options = {}
  const { noExpandExternal } = options
  if (options.debug) log.enable()
  // log.debug('getDataCallback avec l’arbre', arbre, 'et les options', options)

  return dataCallback
}

/**
 * Ajoute un arbre jstree dans le dom
 * Il faut charger les css séparément, par ex en incluant le module addCss pour que wepback les compile
 * Attention, pour écouter les événements jstree, il faut impérativement, au choix
 * - charger jQuery d'abord et le mettre dans window.jQuery
 * - charger jQuery d'abord et le mettre dans options.jQuery
 *   (cette fct va le mettre en global pour que jstree utilise la même instance)
 * - utiliser le window.jQuery qui existera après résolution de la promesse
 * Sinon $(document).on('dnd_xxxx.vakata') ou $.on('xxx.jstree') ne sera jamais appelé chez vous !
 * @param {HTMLElement} elt
 * @param {Ressource} arbre
 * @param {object} options
 * @param {function} [options.errorCallback] Sera ajouté si ça n'existe pas (avec un div pour l'afficher ajouté aussi)
 * @param {boolean|function} [options.check_callback=false] cf https://www.jstree.com/api/#/?q=check_callback&f=$.jstree.defaults.core.check_callback)
 * @param {object} [options.contextmenu] pour ajouter des actions au clic droit sur les items,
 *                    cf https://www.jstree.com/api/#/?q=$.jstree.defaults.contextmenu
 * @param {boolean} [options.debug] si true on affiche la stack d'erreur éventuelle en console
 * @param {object} [options.dnd] Si présent et dnd pas dans plugins on l'ajoutera
 *                     pour le contenu possible cf https://www.jstree.com/api/#/?q=dnd&f=$.jstree.defaults.dnd
 * @param {function} [options.errorCallback] sera rappelée avec une erreur en cas de pb de chargement,
 *                              sinon l'erreur sera affichée au dessus de l'arbre
 * @param {object} [options.listeners]  passer une liste de event=>listener du genre`{'eventFoo.jstree': cb1, 'eventBar.jstree': cb2, …}`
 * @param {object[]} [options.plugins] array passé à jstree, cf https://www.jstree.com/plugins/
 * @param {number} [options.timeout] delai max en ms pour charger les enfants
 * @return {Promise<jQueryElement>} Le wrapper jQuery de l'arbre (sera ≠ $(elt) si on a pas passé d'option errorCallback)
 */
export function build (elt, arbre, options) {
  log.debug('build avec l’arbre', arbre, 'et les options', options)

  if (typeof window === 'undefined') {
    return Promise.reject(Error('Cette méthode ne peut fonctionner que dans un navigateur'))
  }

  return Promise.resolve().then(() => {
    if (!options) options = {}
    const $ = options.jQuery || window.jQuery
    if ($) return $
    return import('jquery')
  }).then((jQuery) => {
    // on peut le récupérer directement, ou via l'import (dans ce cas c'est la propriété default qu'on veut)
    window.jQuery = jQuery.default || jQuery
    if (jQuery.jstree) return
    // jstree (au moins en 3.3.4) prend jQuery en global
    return import('jstree')
  }).then(() => {
    if (!window.jQuery || !window.jQuery.jstree) throw Error('jstree n’a pas été correctement chargé')
    const $ = window.jQuery
    // rien au double clic (toggle déjà au simple clic)
    $.jstree.defaults.core.dblclick_toggle = false

    // Vu qu'on reconstruit un nouveau conteneur si y'a pas de callback d'erreur,
    // faut qu'on mémorise l'élément qu'on nous passe pour le lier au conteneur qu'on a peut-être
    // créé la dernière fois. C'est indispensable pour
    // - supprimer l'arbre avant d'en reconstruire un autre (sinon la reconstruction marche pas, et jstree dit rien)
    // - conserver le $errorContainer associé à elt
    let $container
    const memo = builded.get(elt)
    if (memo) {
      $container = memo[0]
      // si on nous passe une nouvelle fct errorCallback elle prend le dessus sur celle qu'on avait stockée la dernière fois
      if (!options.errorCallback) options.errorCallback = memo[1]
      // si on avait un conteneur d'erreur mémorisé pour cet élément (donc construit ici), on le vide
      if (memo[2]) memo[2].empty() // c'est $errorContainer donc avec la méthode jQuery empty
    } else {
      // c'est la 1re fois qu'on nous passe cet elt
      $container = $(elt)
      let $errorContainer
      // si on nous fourni pas de callback d'erreur on la gère nous-même
      if (!options.errorCallback) {
        // on crée 2 div dans l'ancien $container (un pour les erreurs et l'autre pour le nouveau $container)
        $errorContainer = $('<div></div>').appendTo($container)
        $container = $('<div></div>').appendTo($container)
        options.errorCallback = function errorCallback (error) {
          if (!error) return console.error(Error('errorCallback appelé sans erreur à afficher'))
          if (options.debug && error.stack) $errorContainer.append(`<pre class="error">${error.stack}</pre>`)
          else $errorContainer.append(`<p class="error">${error}</p>`)
        }
      }
      builded.set(elt, [$container, options.errorCallback, $errorContainer])
    }

    // si on ne détruit pas un éventuel jstree existant il refuse d'en charger un autre
    const treeRef = $.jstree.reference($container)
    if (treeRef) treeRef.destroy()

    // on peut charger un arbre
    const jstData = {
      core: {
        check_callback: options.check_callback || false,
        data: getDataCallback(arbre, options)
      }
    }
    // plugins
    if (options.plugins && Array.isArray(options.plugins)) jstData.plugins = options.plugins
    // drag & drop
    if (options.dnd) {
      jstData.dnd = options.dnd
      // et on ajoute le plugin à la liste s'il n'y est pas
      if (jstData.plugins) {
        if (!jstData.plugins.includes('dnd')) jstData.plugins.push('dnd')
      } else {
        jstData.plugins = ['dnd']
      }
    }
    // clic droit
    if (options.contextmenu) {
      jstData.contextmenu = options.contextmenu
      // et on ajoute le plugin si ce n'est pas déjà fait
      if (jstData.plugins) {
        if (jstData.plugins.indexOf('contextmenu') === -1) jstData.plugins.push('contextmenu')
      } else {
        jstData.plugins = ['contextmenu']
      }
    }
    // listeners
    if (options.listeners) {
      Object.entries(options.listeners).forEach(([eventName, listener]) => {
        $container.on(eventName, listener)
      })
    }

    $container.jstree(jstData)

    // on lui ajoute ça pour le récupérer si besoin
    $container.rid = arbre.rid || arbre.aliasOf

    return $container
  })
}

/**
 * Ajoute un node à un arbre déjà dans le dom
 * @param {jqObject} $container Le conteneur jQuery de l'arbre (retourné par build)
 * @param {Ressource|Ref|string} ressource ou id
 * @param {object} [options]
 * @param {string} [options.parentRef=#] L'id jstree du parent auquel ajouter ce node
 * @param {string} [options.pos=last] La position dans le parent
 * @param {simpleCallback} next appelé avec (error, newNode) une fois le node chargé
 */
export function addNode ($container, ressource, options = {}, next) {
  function create () {
    instance.create_node(parent, toJstree(ressource), pos, function (newNode) {
      next(null, newNode)
    })
  }
  // compatibilité ascendante, avant le 3e param était parentRef
  const parentRef = (typeof options === 'object' && options.parentRef) || options || '#'
  const pos = (typeof options === 'object' && typeof options.pos === 'number' && options.pos > -1)
    ? options.pos // ça peut être 0, donc un if (option.pos) suffit pas dans le test précédent
    : 'last'
  const instance = $container.jstree(true)
  if (!instance) throw Error('Arbre introuvable')
  const parent = instance.get_node(parentRef)
  if (!parent) throw Error('Référence du parent introuvable')
  if (typeof ressource === 'string') {
    fetchRef(ressource, function (error, ref) {
      if (error) return next(error)
      ressource = ref
      create()
    })
  } else {
    create()
  }
}

/**
 * Retourne un node sous forme de Enfant
 * Cette méthode n'est pas utilisé par le plugin lui-même mais par d'autres (sesaparcours:src/editgraphes/menu par ex
 * @param {jqObject} $container Le conteneur jQuery de l'arbre (retourné par build)
 * @param {string} id L'id jstree du node que l'on veut récupérer, passer '#' pour récupérer l'arbre complet
 */
export function getAsEnfant ($container, jstId) {
  const instance = $container.jstree(true)
  if (!instance) throw Error('Arbre jstree introuvable dans cet élément')
  if (jstId === '#') {
    // la racine est un cas particulier où on veut le 1er et unique enfant
    const root = instance.get_node(jstId)
    if (root.children.length !== 1) throw Error('Arbre invalide')
    return toEnfant(instance, root.children[0].id)
  }
  return toEnfant(instance, jstId)
}
// Ancien nom de la méthode que l'on conserve pour compatibilité ascendante (ça retournera moins de propriétés qu'avant mais ne plantera pas)
export const getAsRef = getAsEnfant

/**
 * Retourne le tableau des enfants d'un node jstree sous forme de Enfant[]
 * @param {object} jstree L'instance jstree retourné par `$container.jstree(true)` ($container étant retourné par build)
 * @param {string} [parentId=#] Le nodeId dont on veut les enfants, '#' par défaut pour la racine
 * @return {Enfant[]} Le tableau des enfants au format Enfant
 */
export function getEnfants (jstree, parentId = '#') {
  const parent = jstree.get_node(parentId)
  if (!parent) throw Error(`Référence du parent introuvable (${parentId})`)
  log.debug('getEnfants de', parent)
  return parent.children.map((childId) => toEnfant(jstree, childId))
}

export function getPos (inst, nodeRef) {
  const node = inst.get_node(nodeRef)
  const parent = inst.get_node(node.parent)
  return parent.children.indexOf(node.id)
}
