/**
 * Valide un arbre (ajoutera éventuellement une string dans errors.enfants)
 * @param {Ressource} arbre
 * @param {Object} errors
 * @param {string} errors.enfants L'erreur éventuelle qui sera ajoutée en cas de pb de validation
 */
const validate = (arbre, errors) => {
  let children = arbre.enfants
  if (typeof children === 'string') {
    try {
      children = JSON.parse(children)
    } catch (err) {
      errors.enfants = 'Le JSON est invalide'
      return
    }
  }

  const getError = (enfants) => {
    if (!Array.isArray(enfants)) {
      return 'Une propriété enfants n’est pas un tableau'
    }
    let enfant
    for (enfant of enfants) {
      if (typeof enfant !== 'object') {
        return 'Les enfants doivent être des objets'
      }
      const { type, titre, aliasOf } = enfant
      if (typeof titre !== 'string' || !titre.length) {
        return 'Un enfant doit avoir un titre'
      }
      if (typeof type !== 'string' || !type.length) {
        return 'Un enfant doit avoir un type'
      }
      // pour les arbres on autorise un dosssier vide (pas de propriété enfants)
      // et on ne parse pas les alias (qui ne devraient pas non plus avoir d'enfants)
      if (type === 'arbre' && !aliasOf && enfant.enfants) {
        const error = getError(enfant.enfants)
        if (error) return error
        // sinon on continue le for
      }
    }
  }

  const error = getError(children)

  if (error) {
    console.error(Error(error), arbre.enfants)
    errors.enfants = error
  }
}

export default validate
