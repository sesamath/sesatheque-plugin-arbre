const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { version } = require('./package')

module.exports = () => ({
  entries: {
    // le js chargé par /public/edit.html (mis en iframe)
    editArbre: path.resolve(__dirname, 'editArbre.js'),
    diffArbres: path.resolve(__dirname, 'diffArbres.js')
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{
        from: path.resolve(__dirname, 'public', 'edit.html'),
        to: 'plugins/arbre/',
        // content est un Buffer
        transform: (content) => content.toString().replace('#editArbre#', `/editArbre.js?${version}`)
      }, {
        from: path.resolve(__dirname, 'public', 'diffArbres.html'),
        to: 'plugins/arbre/',
        transform: (content) => content.toString().replace('#diffArbres#', `/diffArbres.js?${version}`)
      }, {
        from: path.resolve(__dirname, 'public', 'images', 'arbre.gif'),
        to: 'plugins/arbre/'
      }]
    })
  ],
  rules: []
})
